package avi.ru.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static avi.ru.calculator.R.id.first_summand;


public class MainActivity extends AppCompatActivity {
    String first_summand;
    String second_summand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.summary_button);

        
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                EditText editText = (EditText) findViewById(R.id.first_summand);
                EditText editText_2 = (EditText) findViewById(R.id.second_summand);
                first_summand = editText.getText().toString();
                if (first_summand.isEmpty()) {
                    editText.setError("Пусто");
                    return;
                }

                second_summand = editText_2.getText().toString();
                if (second_summand.isEmpty()) {
                    editText_2.setError("Пусто");
                    return;
                }
                intent.putExtra("first_summand", first_summand);
                intent.putExtra("second_summand", second_summand);
                startActivity(intent);
            }
        });
    }


}
