package avi.ru.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = new Intent(getIntent());
        String first_summand = intent.getStringExtra("first_summand");
        String second_summand = intent.getStringExtra("second_summand");

        TextView textView = (TextView) findViewById(R.id.first_summand);
        TextView textView_2 = (TextView) findViewById(R.id.second_summand);

        textView.setText(first_summand);
        textView_2.setText(second_summand);

        summary(first_summand, second_summand);


    }

    public void summary(String first_summand, String second_summand) {
        TextView textView = (TextView) findViewById(R.id.result);
        Integer summ = Integer.parseInt(first_summand) + Integer.parseInt(second_summand);
        textView.setText(summ.toString());
    }

}
